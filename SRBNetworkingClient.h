#import <Foundation/Foundation.h>
#import "SRBNetworkingRequest.h"
#import "SRBNetworkingCacheProtocol.h"

@interface SRBNetworkingClient : NSObject

@property (nonatomic, readonly)		NSOperationQueue				*operationQueue;		// useful for tweaking settings in the opQ
@property (nonatomic, strong)		id<SRBNetworkingCacheProtocol>	cache;					// optional; requests with shouldCacheResult will use this supplied cache (NSCache 'compatible')

-(void) sendAsyncronousRequest: (SRBNetworkingRequest *) request completionBlock: (SRBNetworkingRequestCompletionBlock) completionBlock;
-(void) sendAsyncronousRequest: (SRBNetworkingRequest *) request completionBlock: (SRBNetworkingRequestCompletionBlock) completionBlock completionQueue: (dispatch_queue_t) completionQueue;

// overrides
-(void) requestDidComplete: (SRBNetworkingRequest *) request;								// optional; always call super if overriding; useful for subclasses that need to cull application level error information or common results from the request

@end


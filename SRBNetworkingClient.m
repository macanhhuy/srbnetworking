#import "SRBNetworking.h"

@implementation SRBNetworkingClient

@synthesize operationQueue = _operationQueue;
@synthesize cache;

-(id) init
{
	if( (self = [super init]) )
	{
		_operationQueue = [NSOperationQueue new];
	}
	return self;
}

-(void) sendAsyncronousRequest: (SRBNetworkingRequest *) request completionBlock: (SRBNetworkingRequestCompletionBlock) completionBlock
{
	[self sendAsyncronousRequest: request completionBlock: completionBlock completionQueue: request.requestCompletionQueue];
}

-(void) sendAsyncronousRequest: (SRBNetworkingRequest *) request completionBlock: (SRBNetworkingRequestCompletionBlock) completionBlock completionQueue: (dispatch_queue_t) completionQueue
{
	request.requestCompletionBlock = completionBlock;
	request.requestCompletionQueue = completionQueue;
	
	__weak SRBNetworkingRequest *weakRequest = request;
	__weak SRBNetworkingClient *weakSelf = self;
	
	// process the response via the NSOperation -completionBlock
	request.completionBlock = ^{
		if( !weakRequest.isCancelled )
		{
			[weakSelf requestDidComplete: weakRequest];
		}
	};
	
	// cache check 
	BOOL satisfiedWithCache = NO;
	if( request.shouldCacheResult && self.cache )
	{
		id resultsData = [self.cache objectForKey: [request.urlRequest.URL absoluteString]];
		if( resultsData )
		{
			request.resultsData = resultsData;
			request.resultsError = nil;
			request.shouldCacheResult = NO;			// since we satisfied from cache, disable the saving to cache here.
			request.wasSatisfiedViaCache = YES;
			satisfiedWithCache = YES;
			
			[request requestDidComplete];
		}
	}
	
	if( satisfiedWithCache )
	{
		// this is done on a sep queue since the save to cache might be expensive...
		dispatch_async( dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) , ^{
			[self requestDidComplete: request];
		});
	}
	else
	{
		// async since the caller might want to continue configuring the request on the current runloop cycle before it goes in-flight
		dispatch_async( dispatch_get_current_queue(), ^{
			[self.operationQueue addOperation: request];
		});
	}
}

#pragma mark- Overrides 

-(void) requestDidComplete:(SRBNetworkingRequest *)request
{
	// if we should cache, save off the result
	if( request.shouldCacheResult && self.cache && request.resultsData && [request.urlRequest.URL absoluteString] )
	{
		[self.cache setObject: request.resultsData forKey: [request.urlRequest.URL absoluteString]];
	}
	
	if( request.requestCompletionBlock && !request.isCancelled ) 
	{
		dispatch_async(request.requestCompletionQueue, ^{
			// fire our completion blocks			
			request.requestCompletionBlock( request );
		});
	}
}

@end

